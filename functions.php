<?php

add_theme_support( 'post-thumbnails' );

add_image_size( "blog", 800, 400, true );
add_image_size( "blog-mini", 400, 200, true );

load_theme_textdomain( "vuelo", get_template_directory()."/lang" );

include dirname(__FILE__) . '/inc/customfields.php';
include dirname(__FILE__) . '/vuelo-ajax.php';
include dirname(__FILE__) . '/inc/faq.php';
include dirname(__FILE__) . '/inc/simple-custom-post-order/simple-custom-post-order.php';

function wpdocs_theme_name_scripts() {
	global $wp_query;

	/* add jquery */
	wp_enqueue_script ( 'jquery', get_template_directory_uri() . '/_assets/js/jquery.min.js' );

	/* add bootstap */
	wp_enqueue_style ( 'bootstap-css', get_template_directory_uri() . '/_assets/css/bootstrap.min.css' );
	wp_enqueue_script( 'bootstap-js', get_template_directory_uri() . '/_assets/js/bootstrap.min.js');

	/* add font-awesome */
	wp_enqueue_style ( 'font-awesome', get_template_directory_uri() . '/_assets/css/font-awesome.min.css' );

	/* add owl-carousel */
	wp_enqueue_style ( 'owl-css', get_template_directory_uri() . '/_assets/css/owl.carousel.css' );
	wp_enqueue_style ( 'owl-theme', get_template_directory_uri() . '/_assets/css/owl.theme.css' );
	wp_enqueue_script( 'owl-js', get_template_directory_uri() . '/_assets/js/owl.carousel.js');

	/* add Typed.js */
	wp_enqueue_script( 'Typed-js', get_template_directory_uri() . '/_assets/js/typed.min.js');

	/* add wow.js */
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/_assets/js/wow.min.js');

	/* add animate */
	wp_enqueue_style ( 'animate-css', get_template_directory_uri() . '/_assets/css/animate.min.css' );
	wp_enqueue_script ( 'mask', get_template_directory_uri() . '/_assets/js/jquery.mask.min.js' );

	/* add hover */
	wp_enqueue_style ( 'animate-css', get_template_directory_uri() . '/_assets/css/hover.css' );

	/* add cidades-estados-js */
	wp_enqueue_script( 'cidades-estados-js', get_template_directory_uri() . '/_assets/js/cidades-estados-v0.2.js');

	/* add vivus-js */
	wp_enqueue_script( 'vivus-js', get_template_directory_uri() . '/_assets/js/vivus.min.js');

	/* add matchHeight-js */
	wp_enqueue_script( 'matchHeight-js', get_template_directory_uri() . '/_assets/js/jquery.matchHeight-min.js');

	/* add parallax-js */
	wp_enqueue_script( 'parallax-js', get_template_directory_uri() . '/_assets/js/parallax.min.js');

	/* add jquery.validate-js */
	wp_enqueue_script( 'jquery.validate-js', get_template_directory_uri() . '/_assets/js/jquery.validate.min.js');

	/* add css|js */
	wp_enqueue_style ( 'css', get_template_directory_uri() . '/_assets/css/all.min.css' );
	wp_enqueue_script( 'js', get_template_directory_uri() . '/_assets/js/app.min.js');

	$perfil = get_page_by_title("Perfil"); 
	$perfil = get_permalink($perfil->ID);

	$blog = get_page_by_title("Blog"); 
	$blog = get_permalink($blog->ID);

	wp_localize_script(
		'js',
		'vuelo',
		array(
			'template' 		=> get_bloginfo('template_url'), 
			'url' 			=> get_bloginfo('url'),
			'query_vars' 	=> json_encode( $wp_query->query ),
			'perfil_url'	=> $perfil,
			'blog_url'		=> $blog,
			'ajaxurl' 		=> admin_url('admin-ajax.php'),
			'current_url'	=> $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]
		)
	);

}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

function google_fonts() {
	wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i|Oswald:300,400,700');
	wp_enqueue_style( 'googleFonts');
}
add_action('wp_print_styles', 'google_fonts');

function the_slug($echo = true) {
	$slug = basename(get_permalink());
	do_action('before_slug', $slug);
	$slug = apply_filters('slug_filter', $slug);
	if ($echo) echo $slug;
	do_action('after_slug', $slug);

	return $slug;
}

function javascript_do_ajax() {
	$script  = '<script>var ajaxurl = "' . admin_url('admin-ajax.php') . '";</script>';
	echo $script;
}
add_action( 'wp_footer', 'javascript_do_ajax' ); 

function my_function_admin_bar(){
  return false;
}
add_filter( "show_admin_bar" , "my_function_admin_bar");