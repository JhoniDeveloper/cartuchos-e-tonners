<?php
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	global $product;

	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}
?>

<div itemscope id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
		<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>

	<section class="section-produtos spray">

		<nav class="nav-highlight">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-7">
					<ul>
						<li>
							<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/spray-1.svg" alt="">
							<em>
								 <?php _e("Aplicável de", "vuelo"); ?> <br>
								<?php _e("qualquer ângulo", "vuelo"); ?>
							</em>
						</li>
						<li>
							<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/spray-2.svg" alt="">
							<em>
								<?php _e("Protege por", "vuelo"); ?> <br>
								<?php _e("por até 72 horas", "vuelo"); ?>
							</em>
						</li>
						<li>
							<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/spray-3.svg" alt="">
							<em>
								<?php _e("Regenera a barreira", "vuelo"); ?> <br> 
								<?php _e("cutânea e não arde", "vuelo"); ?>
							</em>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-5">
					<ol> 
						<?php $distribuidores = get_page_by_title("Distribuidores"); ?>
						<li><a href="<?php echo site_url(); ?>/distribuidores-vuelo"><?php _e("Encontre um Distribuidor", "vuelo"); ?></a></li>
                        <li><a href="http://loja.membracel.com.br" target="_blank" class="solid"><?php _e("Compre On-line", "vuelo"); ?></a></li>
					</ol>
				</div>	
			</div>
		</nav>

		<aside class="produto-content">
			<div class="row">
				<div class="col-md-6 gallery">

					<?php if ($product->get_gallery_attachment_ids()): ?>
					<div class="gallery-slider">
						<?php foreach ($product->get_gallery_attachment_ids() as $id): ?>
						<div class="item">
							<?php $attachment = get_post( $id ); ?>
							<img src="<?php echo wp_get_attachment_url( $id ); ?>" alt="">
							<?php if ($attachment->post_excerpt): ?>
								<span><?php echo $attachment->post_excerpt; ?></span>
							<?php endif ?>
						</div>
						<?php endforeach ?>
					</div>	
					<?php endif ?>

					<div class="row">
						<div class="col-xs-6"><img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/thumbnail-spray-1.jpg"></div>
						<div class="col-xs-6"><img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/thumbnail-spray-2.jpg"></div>
					</div>
				</div>
				<div class="col-md-6">
					<article class="produto-description">
						<p class="bula">
							<a href="<?php echo site_url(); ?>/wp-content/uploads/2016/12/Spray-de-Barreira.pdf" target="_blank">
								<i class="fa fa-file-o"></i> <?php _e("Bula em PDF", "vuelo"); ?>
							</a>
						</p>

						<h1 class="produto-title">
							<?php _e("Spray de Barreira", "vuelo"); ?> 
							<span><?php _e("Protetor Cutâneo", "vuelo"); ?></span>
						</h1>

						<p><?php _e("Protetor Cutâneo VUELO é uma solução polimérica líquida de secagem rápida, incolor, não gordurosa, resistente à água e sem álcool que forma uma película protetora e uniforme quando aplicado na pele, sem provocar irritações. Dessa forma protege-a por até 72 horas de lesões decorrentes de incontinências urinárias e/ou fecais, sucos digestivos (estomias), fricção e adesivos. O Protetor Cutâneo VUELO pode ser aplicado na pele saudável, irritada ou lesionada, sem causar ardência ou desconforto. Dermatologicamente testado.", "vuelo"); ?></p>

						<div class="produtos-description-accordion">
							<dl>
								<dt><i class="fa fa-medkit"></i> <?php _e("INDICAÇÕES DE USO", "vuelo"); ?></dt>
								<dd>
									<p><?php _e("Proteção da pele ao redor de estomias, fístulas e feridas drenantes; lesões de pele decorrentes de incontinências urinárias e/ou fecais, sucos digestivos (estomias), fricção; processos alérgicos a adesivos (fitas); peri-estomas; ao redor de cânulas de entubação, traqueostomias, gastrostomias; dermatite e irritação de pele; cisalhamento e agressões de adesivos devido trocas constantes de curativos e/ou bolsas.", "vuelo"); ?></p>
								</dd>
							</dl>

							<dl>
								<dt><i class="fa fa-info-circle"></i> <?php _e("INSTRUÇÕES DE USO", "vuelo"); ?></dt>
								<dd>
									<p><?php _e("A pele deve estar limpa e seca antes da aplicação do Protetor Cutâneo VUELO. Mantenha o aplicador a uma distância de 10 a 15 cm da pele e pulverize uma camada lisa e uniforme de película na região que requer proteção. Deixe secar completamente. Se uma segunda camada for necessária, deixe a primeira secar antes da reaplicação. É indicada a aplicação do Protetor Cutâneo VUELO a cada 72 horas. Não é necessário remover a película existente antes da reaplicação. 
									Nos casos de incontinência urinária e/ou fecal ou casos em que houver a necessidade de limpezas frequentes, a reaplicação é indicada a cada 24 horas ou quando se fizer necessário. Para proteção sob adesivos, reaplique a cada troca de fitas adesivas ou curativos. Caso deseje, a película poderá ser removida com um solvente de adesivos de uso hospitalar, de acordo com as instruções de uso do solvente, evitando a fricção da pele para não causar lesões.", "vuelo"); ?></p>
								</dd>
							</dl>

							<dl>
								<dt><i class="fa fa-exclamation-triangle"></i> <?php _e("PRECAUÇÕES E ADVERTÊNCIAS", "vuelo"); ?></dt>
								<dd>
									<p><?php _e("Suspender o uso caso ocorra eritema ou qualquer outro sinal de irritação e, caso os sintomas persistam, consulte seu médico. O uso concomitante de outros produtos de proteção como cremes, loções e pomadas pode diminuir a eficácia do Protetor Cutâneo VUELO.", "vuelo"); ?></p>
								</dd>
							</dl>
						</div>

					</article>
				</div>
			</div>
		</aside>

		<?php get_template_part("inc/banner", "highlight"); ?>

	</section>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>