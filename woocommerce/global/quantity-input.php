<?php

if ( ! defined( 'ABSPATH' ) ) exit;
 
global $product;
 
$defaults = array(
	'max_value'   => apply_filters( 'woocommerce_quantity_input_max', '', $product ),
	'min_value'   => apply_filters( 'woocommerce_quantity_input_min', '', $product ),
	'step'        => apply_filters( 'woocommerce_quantity_input_step', '1', $product ),
);
 
if ( ! empty( $defaults['min_value'] ) )
	$min = $defaults['min_value'];
else $min = 1;
 
if ( ! empty( $defaults['max_value'] ) )
	$max = $defaults['max_value'];
else $max = 10;
 
if ( ! empty( $defaults['step'] ) )
	$step = $defaults['step'];
else $step = 1;
 
?>
<div class="quantity-select">
	<select name="<?php echo esc_attr( $input_name ); ?>" class="form-control">
	<?php
		for ( $count = $min; $count <= $max; $count = $count+$step ) {
			$selected = ( $count == $input_value ) ? ' selected' : '';
			echo '<option value="' . $count . '"' . $selected . '>' . $count . '</option>';
		}
	?>
	</select>
</div>