<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $breadcrumb ) ) {

	echo $wrap_before;

	foreach ( $breadcrumb as $key => $crumb ) {

		echo $before;

		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			$label = (esc_html( $crumb[0] ) == "Início") ? '<i class="fa fa-home"></i>' : esc_html( $crumb[0] );
			echo '<a href="' . esc_url( $crumb[1] ) . '">' . $label . '</a>';
		} else {
			echo esc_html( $crumb[0] );
		}

		echo $after;

		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<span> <i class="fa fa-angle-right"></i> </span>';
		}

	}

	echo $wrap_after;

}
