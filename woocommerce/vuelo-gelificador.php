<?php
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	global $product;

	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}
?>

<div itemscope id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
		<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>

	<section class="section-produtos gelificador">

		<nav class="nav-highlight">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-7">
					<ul>
						<div class="row header-comprar">
							<li class="icon-1">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/gelificador-1.svg" alt="">
								<em>
									<?php _e("Fragrância", "vuelo"); ?> <br>
									<?php _e("de lavanda", "vuelo"); ?>
								</em>
							</li>
							<li class="icon-2">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/gelificador-2.svg" alt="">
								<em>
									<?php _e("Reduz", "vuelo"); ?> <br>
									<?php _e("ruídos", "vuelo"); ?>
								</em>
							</li>
							<li class="icon-3">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/gelificador-3.svg" alt="">
								<em>
									<?php _e("Evita", "vuelo"); ?> <br>
									<?php _e("vazamentos", "vuelo"); ?>
								</em>
							</li>
							<li class="icon-4">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/gelificador-4.svg" alt="">
								<em>
									<?php _e("Gelifica", "vuelo"); ?> <br>
									<?php _e("com cor", "vuelo"); ?>
								</em>
							</li>
						</div>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-5">
					<ol> 
						<?php $distribuidores = get_page_by_title("Distribuidores"); ?>
						<li><a href="<?php echo site_url(); ?>/distribuidores-vuelo"><?php _e("Encontre um Distribuidor", "vuelo"); ?></a></li>
                        <li><a href="http://loja.membracel.com.br" target="_blank" class="solid"><?php _e("Compre On-line", "vuelo"); ?></a></li>
					</ol>
				</div>	
			</div>
		</nav>

		<aside class="produto-content">
			<div class="row">
				<div class="col-md-6 gallery">
				
					<?php if ($product->get_gallery_attachment_ids()): ?>
					<div class="gallery-slider">
						<?php foreach ($product->get_gallery_attachment_ids() as $id): ?>
						<div class="item">
							<?php $attachment = get_post( $id ); ?>
							<img src="<?php echo wp_get_attachment_url( $id ); ?>" alt="">
							<?php if ($attachment->post_excerpt): ?>
								<span><?php echo $attachment->post_excerpt; ?></span>
							<?php endif ?>
						</div>
						<?php endforeach ?>
					</div>	
					<?php endif ?>

					<div class="row">
						<div class="col-xs-6"><img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/thumbnail-1-gelific.jpg"></div>
						<div class="col-xs-6"><img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/thumbnail-2-gelific.jpg"></div>
					</div>
				</div>
				<div class="col-md-6">
					<article class="produto-description">
						<p class="bula">
							<a href="<?php echo site_url(); ?>/wp-content/uploads/2016/12/Gelificador.pdf" target="_blank">
								<i class="fa fa-file-o"></i> <?php _e("Bula em PDF", "vuelo"); ?>
							</a>
						</p>
						<h1 class="produto-title">
							<?php _e("Gelificador", "vuelo"); ?>
							<span><?php _e("Para Bolsas de Estomia", "vuelo"); ?></span>
						</h1>

						<p><?php _e("O Gelificador para bolsas de estomia VUELO é constituído de grânulos de polímero acrílico, com grande capacidade de absorção, que permite a gelificação do líquido existente no interior da bolsa, diminuindo o odor, ruídos ou possíveis vazamentos. Quando em contato com líquido apresenta coloração. Possui fragrância agradável de óleo essencial de lavanda.", "vuelo"); ?></p>

						<dl>
							<dt><i class="fa fa-medkit"></i> <?php _e("INDICAÇÕES DE USO", "vuelo"); ?></dt>
							<dd>
								<p><?php _e("Gelificar urina e fezes líquidas no interior de qualquer modelo de bolsa de estomia.", "vuelo"); ?></p>
							</dd>
						</dl>

						<dl>
							<dt><i class="fa fa-info-circle"></i> <?php _e("INSTRUÇÕES DE USO", "vuelo"); ?></dt>
							<dd>
								<p><?php _e("Após o esvaziamento, a limpeza ou a troca da bolsa de estomia, insira uma ou mais cápsulas do Gelificador VUELO no interior da bolsa. Após o rompimento da cápsula e em contato com os fluídos corpóreos, o líquido no interior da bolsa se transformará em gel. Uma cápsula é suficiente para gelificar até 100 ml de líquido.", "vuelo"); ?></p>
							</dd>
						</dl>

						<dl>
							<dt><i class="fa fa-exclamation-triangle"></i> <?php _e("PRECAUÇÕES E ADVERTÊNCIAS", "vuelo"); ?></dt>
							<dd>
								<p><?php _e("Em caso de irritação, suspenda o uso e consulte seu médico.", "vuelo"); ?> <strong><?php _e("Manter fora do alcance de crianças. Não ingerir.", "vuelo"); ?></strong></p>
							</dd>
						</dl>

					</article>
				</div>
			</div>
		</aside>

		<?php get_template_part("inc/banner", "highlight"); ?>

	</section>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>