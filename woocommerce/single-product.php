<?php

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	get_header( 'shop' );

	do_action( 'woocommerce_before_main_content' );
		while ( have_posts() ) : the_post();
			switch ($post->post_name) {
				case 'gelificador-para-bolsas-de-estomia':
					wc_get_template_part( 'vuelo', 'gelificador' );
					break;

				case 'spray-de-barreira-protetor-cutaneo':
					wc_get_template_part( 'vuelo', 'spray' );
					break;

				case 'membracel-membrana-regeneradora-porosa':
					wc_get_template_part( 'vuelo', 'membracel' );
					break;

				case 'membracel-membrana-regeneradora-sem-poros':
					wc_get_template_part( 'vuelo', 'membracel' );
					break;
				
				default:
					wc_get_template_part( 'content', 'single-product' );
					break;
			}
		endwhile;

	do_action( 'woocommerce_after_main_content' );

	 get_footer( 'shop' );

 ?>
