<?php
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	global $product;

	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}

?>

<div itemscope id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
		<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>

	<section class="section-produtos membracel">

		<nav class="nav-highlight">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-7">
					<ul>
						<div class="row header-comprar">
							<li class="icon-1">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/membracel-1.svg" alt="">
								<em>
									<?php _e("Diminui", "vuelo"); ?> <br>
									<?php _e("a dor", "vuelo"); ?>
								</em>
							</li>
							<li class="icon-2">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/membracel-2.svg" alt="">
								<em>
									<?php _e("Fácil", "vuelo"); ?> <br>
									<?php _e("aplicação", "vuelo"); ?>
								</em>
							</li>
							<li class="icon-3">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/membracel-3.svg" alt="">
								<em>
									<?php _e("Rápida", "vuelo"); ?> <br>
									<?php _e("cicatrização", "vuelo"); ?>
								</em>
							</li>
							<li class="icon-4">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/membracel-4.svg" alt="">
								<em>
									<?php _e("Ótimo", "vuelo"); ?> <br>
									<?php _e("custo-benefício", "vuelo"); ?>
								</em>
							</li>
						</div>
					</ul>	
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-5">
					<ol> 
						<?php $distribuidores = get_page_by_title("Distribuidores"); ?>
						<li><a href="<?php echo site_url(); ?>/distribuidores-vuelo"><?php _e("Encontre um Distribuidor", "vuelo"); ?></a></li>
							 	<span class="menu-icon open-search-header">
					</ol>
				</div>	
			</div>
		</nav>

		<aside class="produto-content">
			<div class="row">
				<div class="col-md-6 gallery">
					<?php if ($product->get_gallery_attachment_ids()): ?>
					<div class="gallery-slider">
						<?php foreach ($product->get_gallery_attachment_ids() as $id): ?>
						<div class="item">
							<?php $attachment = get_post( $id ); ?>
							<img src="<?php echo wp_get_attachment_url( $id ); ?>" alt="">
							<?php if ($attachment->post_excerpt): ?>
								<span><?php echo $attachment->post_excerpt; ?></span>
							<?php endif ?>
						</div>
						<?php endforeach ?>
					</div>	
					<?php endif ?>
					<div class="row hidden-xs">
						<div class="col-xs-6"><img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/fixa1.jpg"></div>
						<div class="col-xs-6"><img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/fixa2.jpg"></div>
					</div>
				</div>
				<div class="col-md-6">
					<article class="produto-description">
						<p class="bula">
							<!-- Porosa -->
							<?php if($post->ID === 125) { ?>
							<a href="<?php echo site_url(); ?>/wp-content/uploads/2016/12/Membrana-Regeneradora-Porosa.pdf" target="_blank">
								<i class="fa fa-file-o"></i> <?php _e("Bula em PDF", "vuelo"); ?>
							</a>
							<?php } ?>
							<!-- Sem Poros -->
							<?php if($post->ID === 130) { ?>
							<a href="<?php echo site_url(); ?>/wp-content/uploads/2016/12/Membrana-Regeneradora.pdf" target="_blank">
								<i class="fa fa-file-o"></i> <?php _e("Bula em PDF", "vuelo"); ?>
							</a>
							<?php } ?>
						</p>
						<h1 class="produto-title">
							<?php the_title(); ?>
						</h1>

						<p><?php _e("Membracel é uma membrana de celulose cristalina capaz de substituir temporariamente a pele. É um curativo biocompatível, inerte, isento de adesivos, atóxico, com textura extremamente fina e com alta resistência no estado úmido. A versão com poros permite as trocas gasosas e a passagem do exsudato para um curativo secundário. Devido às suas características, não se faz necessária a troca diária do produto, evitando possíveis traumas, promovendo o desenvolvimento do tecido de granulação, reduzindo a dor através do isolamento das terminações nervosas e acelerando o processo cicatricial.", "vuelo"); ?></p>

						<div class="show-more-content">
							<p><?php _e("O produto foi desenvolvido para o tratamento da mãe do inventor João Carlos Moreschi, que sofria com úlceras. Inconformado com a falta de resultados efetivos nos tratamentos convencionais, ele resolveu tentar buscar uma solução. João Carlos, que possui conhecimentos científicos e tecnológicos nas áreas de microbiologia e de celulose, decidiu usar sua experiência para desenvolver um produto eficaz e inovador. Após estudos e pesquisas intensivas, chegou ao desenvolvimento de um produto inovador, capaz de eliminar a dor, deixar transpor o exsudato e promover a cicatrização acelerada de lesões da pele: a membrana de celulose, batizada de Membracel.", "vuelo"); ?></p>
						
							<p><?php _e("A Membracel pode ser usada em lesões e feridas de pele como queimaduras, escoriações, lesões por pressão, úlceras arteriais e venosas, feridas do pé diabético, feridas cirúrgicas, lesões causadas por epidermólise bolhosa, lesões pós-cauterização ou laser ou em qualquer outra situação em que ocorra a falta da epiderme ou da derme.", "vuelo"); ?></p>
						</div>
						<button class="show-more-button"><?php _e("Leia Mais", "vuelo"); ?> <span class="fa fa-angle-down"></span></button>

						<hr>

						<ul class="list-info-check">
							<li>
								<i class="fa fa-check-circle"></i>
								<?php _e("Alívio imediato da dor", "vuelo"); ?>;
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<?php _e("Rápida regeneração da pele, acelerando a cicatrização", "vuelo"); ?>;
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<?php _e("Permite trocas gasosas e drenagem do exsudato", "vuelo"); ?>;
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<?php _e("Não deixa resíduos", "vuelo"); ?>;
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<?php _e("Não causa alergia em contato com a pele", "vuelo"); ?>;
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<?php _e("Mantém a área da lesão úmida", "vuelo"); ?>;
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<?php _e("Possibilita a visualização da lesão e o controle da evolução do processo cicatricial.", "vuelo"); ?>
							</li>
						</ul>

					</article>
				</div>
			</div>
		</aside>
		
		<div class="section-como-funciona hidden-xs">
			<div class="row">
				<div class="col-xs-12 col-md-5">
					<h3><?php _e("Como Funciona", "vuelo"); ?></h3>
					<p><?php _e("A Membracel pode ser usada em lesões e feridas de pele como queimaduras, escoriações, lesões por pressão, úlceras arteriais e venosas,
						feridas do pé diabético, feridas cirúrgicas, lesões causadas por epidermólise bolhosa, lesões pós-cauterização ou laser ou em qualquer
					 	outra situação em que ocorra a falta da epiderme ou da derme.", "vuelo"); ?></p>
				</div>
			</div>
			<?php get_template_part("template", "animate"); ?>
	    </div>

		<aside class="produtos-indicacoes">
			<h4><?php _e("Indicações de Uso", "vuelo"); ?></h4>

			<div class="article">
				
				<div class="item">
					<article class="col-xs-12">
						<div class="row">
							<div class="col-sm-6 col-xs-12">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/membrana-porosa.png">
							</div>
							<div class="col-sm-6 col-xs-12">
								<h5><?php _e("Membrana Porosa", "vuelo"); ?></h5>
								<p><?php echo __("<!--:pt-->Funciona como um substituto temporário da pele e é indicada para tratamentos de lesões resultantes da perda do epitélio que sejam caracterizadas como ferimento superficial ou profundo, com <b>exsudação escassa ou abundante</b>, tais como queimaduras de segundo grau, dermoabrasões, escoriações, áreas receptoras e doadoras de enxerto cutâneo, leitos ungueais (pós-exérese ungueal), lesão por pressão, úlceras varicosas de estase, males perfurantes plantares, epidermólise bolhosa, pós-cauterização física (crioterapia, termocauterização, laser CO₂ e Erbium), entre outros.<!--:-->
								<!--:en-->It acts as a temporary replacement of the skin and it is indicated for the treatment of lesions resulting from the loss of the epithelium that are characterized as superficial or deep skin injuries, with low or abundant exudation, such as second degree burns, dermoabrasions, chafing, accepting and donor areas of skin grafts, nail beds (post nail excision), pressure lesions, varicose ulcers of stasis, perforating foot ulcers, epidermolysis bullosa, physical post-cauterization (cryotherapy, thermocauterization, CO2 and Erbium laser), among others.
								<!--:-->
								<!--:es-->Funciona como un sustituto temporario de la piel y se indica para el tratamiento de las lesiones resultantes de la pérdida del epitelio caracterizadas como heridas superficiales o profundas, con exudación escasa o abundante, como por ejemplo quemaduras de segundo grado, dermoabrasiones, escoriaciones, , áreas receptoras y donadoras de injerto cutáneo, Matrices ungueales (pos exéresis ungueal); Úlceras de presión; Males perforantes plantares; heridas quirúrgicas infectadas; Epidermólisis bullosa/ampollosa; Post cauterización física (crioterapia, termo-cauterización, láser CO2 y Erbio), etc.
								<!--:-->"); ?></p>
							</div>
						</div>
						<dl>
							<dt><?php _e("Contraindicações", "vuelo"); ?></dt>
							<dd>
								<p>
									<?php _e("A Membracel não deve ser utilizada para controle de hemorragias, lesões tunelares ou cavitárias, lesões com suspeita de infecção, lesões malignas ou com suspeita de malignidade.", "vuelo"); ?>
								</p>
							</dd>
						</dl>
					</article>
				</div>

				<div class="item">
					<article>
						<div class="row">
							<div class="col-sm-6">
								<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/membrana-nao-porosa.png">
							</div>
							<div class="col-sm-6">
								<h5><?php _e("Membrana Não Porosa", "vuelo"); ?></h5>
								<p><?php _e("<!--:pt-->A Membrana Regeneradora funciona como um substituto temporário da pele e é indicada para tratamentos de lesões resultantes da perda do epitélio, <b>sem exsudação</b>, que sejam caracterizadas como ferimento superficial da pele, como queimaduras da derme, dermo-abrasões, escoriações, áreas receptoras e doadoras de enxerto cutâneo, úlceras de membros inferiores, pós-cauterização física (crioterapia, termocauterização, laser CO2 e Erbium).<!--:-->
								<!--:en-->The Regenerative Membrane acts as a temporary replacement of the skin and it is indicated for the treatment of lesions resulting from the loss of the epithelium, with scarce exudation, which are characterized as superficial skin injuries, such as dermis burns, dermoabrasions, chafing, accepting and donor areas of skin grafts, ulcers of the lower limbs, physical post-cauterization (cryotherapy, thermocauterization, CO2 and Erbium laser). 
								<!--:-->
								<!--:es-->La Membrana Regeneradora actúa como substituto temporario de la piel, se indica para el tratamiento de las lesiones resultantes de la pérdida de epitelio con exudación escasa, que se caracterice como una herida superficial de la piel, como: quemaduras de la piel/dermis, dermoabrasiones, escoriaciones, áreas receptoras y donadoras de injerto cutáneo, úlceras de miembros inferiores; post cauterización física (crioterapia, termo-cauterización, láser CO2 y Erbio).
								<!--:-->"); ?></p>
							</div>
						</div>
						<dl>
							<dt><?php _e("Contraindicações", "vuelo"); ?></dt>
							<dd>
								<p>
									<?php _e("A Membracel sem poros pode ser utilizada em situações similares às indicadas para o uso da Membrana Regeneradora com poros, porém, nesse caso, a lesão deve ser caracterizada como", "vuelo"); ?> 
									<strong><?php _e("ausente de exsudação", "vuelo"); ?></strong> (<?php _e("sem secreção", "vuelo"); ?>).
								</p>
							</dd>
						</dl>
					</article>
				</div>
				
			</div>

		</aside>

		<aside class="produtos-tamanhos">
			<h3><?php _e("Tamanhos disponíveis", "vuelo"); ?></h3>
			<p>
				<?php _e("A escolha correta da Membracel é crucial para o sucesso no tratamento de lesões.", "vuelo"); ?> <br>
				<?php _e("Para isso, adote os seguintes procedimentos", "vuelo"); ?>:
			</p>

			<div class="row">
				<div class="col-md-12">
					<dl class="highlight">
						<dt><?php _e("Escolha do tamanho", "vuelo"); ?>:</dt>
						<dd><?php _e("Considere o tamanho e a forma da ferida para definir a dimensão ideal da membrana, que deverá cobrir toda a área exposta da lesão e ter um excesso mínimo de 1 cm nas bordas.", "vuelo"); ?></dd>
					</dl>
				</div>
			</div>

			<dl class="example">
				<dt><?php _e("Retangulares", "vuelo"); ?></dt>
				<dd>
					<img src="<?php echo get_bloginfo("url"); ?>/wp-content/uploads/2017/01/tamanho-retangularc.png" id='tamanho-mobile' alt="">
					<img src="<?php bloginfo("template_url"); ?>/_assets/img/tamanhos-retangulares.png" id='tamanho-desk' alt="" >
				</dd>
			</dl>

			<div class="row">
				<div class="col-md-6">
					<dl class="example">
						<dt><?php _e("Circulares", "vuelo"); ?></dt>
						<dd><img class="img-circulares" src="<?php echo get_bloginfo("url"); ?>/wp-content/uploads/2017/01/tamanhos-circularesc.png" id="tamanho-mobile"></dd>
						<dd><img class="img-circulares" src="<?php bloginfo("template_url"); ?>/_assets/img/tamanhos-circulares.png" id='tamanho-desk'></dd>
					</dl>

					<dl class="highlight">
						<dt><?php _e("Escolha do PORO", "vuelo"); ?>:</dt>
						<dd>
							<p><?php _e("Verifique o volume e a característica do exsudato (secreção) existente na ferida para escolher o tamanho ideal do poro, conforme abaixo", "vuelo"); ?>:</p>
							<p><?php _e("Poros médios (1 a 2 mm): indicado para exsudação baixa ou moderada. Utilizado na grande maioria dos casos.", "vuelo"); ?></p>
							<p><?php _e("Poros grandes (2 a 3mm): indicado nos casos de exsudação abundante e/ou espessa.", "vuelo"); ?></p>
							<p><?php _e("Sem poros: indicado para feridas sem exsudação.", "vuelo"); ?></p>
						</dd>
					</dl>
				</div>
				<div class="col-md-6 dest">
					<img src="<?php bloginfo("template_url"); ?>/_assets/img/produtos/membracel.jpg">
				</div>
			</div>

		</aside>

	</section>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div>

<div class="modal fade" role="dialog" id="modal-produto">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
				
				<div class="row">
					<div class="col-md-5">
						<figure>
							<?php the_post_thumbnail("full"); ?>
						</figure>
					</div>
					<div class="col-md-7">
						<h2 class="modal-title">
							<?php _e("Membracel Membrana Regeneradora", "vuelo"); ?>
							<span><?php _e("Caixa com 10 unidades", "vuelo"); ?></span>
						</h2>
						<?php woocommerce_template_single_add_to_cart(); ?>

						<nav class="helper-link">
							<a href="<?php bloginfo("url" ); ?>/faq">- <?php _e("Precisa de ajuda com a escolha correta deste produto?", "vuelo"); ?></a> 
							<a href="#">- <?php _e("Baixe a bula deste produto em .pdf", "vuelo"); ?></a>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>