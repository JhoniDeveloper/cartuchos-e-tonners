		</div><!-- .site-content -->

		<script>
			jQuery.extend(jQuery.validator.messages, {
			    required: "<?php _e('Este campo é requerido.', 'vuelo'); ?>",
			    remote: "<?php _e('Por favor, corrija este campo.', 'vuelo'); ?>",
			    email: "<?php _e('Por favor, forneça um endereço eletrônico válido.', 'vuelo'); ?>",
			    url: "<?php _e('Por favor, forneça uma URL válida.', 'vuelo'); ?>",
			    date: "<?php _e('Por favor, forneça uma data válida.', 'vuelo'); ?>",
			    dateISO: "<?php _e('Por favor, forneça uma data válida (ISO).', 'vuelo'); ?>",
			    number: "<?php _e('Por favor, forneça um número válido.', 'vuelo'); ?>",
			    digits: "<?php _e('Por favor, forneça somente dígitos.', 'vuelo'); ?>",
			    creditcard: "<?php _e('Por favor, forneça um cartão de crédito válido.', 'vuelo'); ?>",
			    equalTo: "<?php _e('Por favor, forneça o mesmo valor novamente.', 'vuelo'); ?>",
			    accept: "<?php _e('Por favor, forneça um valor com uma extensão válida.', 'vuelo'); ?>",
			    maxlength: jQuery.validator.format("<?php _e('Por favor, forneça não mais que {0} caracteres.', 'vuelo'); ?>"),
			    minlength: jQuery.validator.format("<?php _e('Por favor, forneça ao menos {0} caracteres.', 'vuelo'); ?>"),
			    rangelength: jQuery.validator.format("<?php _e('Por favor, forneça um valor entre {0} e {1} caracteres de comprimento.', 'vuelo'); ?>"),
			    range: jQuery.validator.format("<?php _e('Por favor, forneça um valor entre {0} e {1}.', 'vuelo'); ?>"),
			    max: jQuery.validator.format("<?php _e('Por favor, forneça um valor menor ou igual a {0}.', 'vuelo'); ?>"),
			    min: jQuery.validator.format("<?php _e('Por favor, forneça um valor maior ou igual a {0}.', 'vuelo'); ?>")
			});
			var aguarde = ['<?php _e("AGUARDE", "vuelo"); ?>'];
		</script>
		
		<footer id="footer" class="site-footer" role="contentinfo">			
			<div id="col-3" class="col-lg-4 col-md-3 col-sm-12">
				<div class="row">
					<div class="widgets_footer">
						<h3>Conteúdo</h3>
						<div class="textwidget">
							<p><a href="<?php echo get_post_type_archive_link("faq"); ?>"><?php _e("Perguntas Frequentes", "vuelo"); ?></a></p>
							<p><a href="<?php bloginfo("url"); ?>/politica-de-entrega"><?php _e("Política de Entrega", "vuelo"); ?></a></p>
							<p><a href="<?php bloginfo("url"); ?>/politica-de-troca-e-devolucao"><?php _e("Política de Troca e Devolução", "vuelo"); ?></a></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="widgets_footer">
						<h3><?php _e("Contato", "vuelo"); ?></h3>
						<div class="textwidget">
							<p><a href="<?php bloginfo("url"); ?>/contato"><?php _e("Fale Conosco", "vuelo"); ?></a></p>
							<p><a href="#" data-toggle="modal" data-target="#modal-curriculo"><?php _e("Trabalhe Conosco", "vuelo"); ?></a></p>
							<div class="contato">
								<a href="tel:+5504136577641">+55 41 3657-7611</a>
								<a href="mailto:contato@vuelopharma.com.br">contato@tonersecartuchos.com.br</a>
							</div>
						</div>		
					</div>			
				</div>
			</div>
			<div class="col-lg-4 col-md-3 col-sm-12 hidden-sm hidden-md hidden-lg coluna-conteudo-mobile">
				<div class="widgets_footer">
					<h3><?php _e("Conteúdo", "vuelo"); ?></h3>
					<div class="textwidget">
						<p><a href="http://vuelo.papricacomunicacao.com.br/faq/"><?php _e("Perguntas Frequentes", "vuelo"); ?></a></p>
						<p><a href="http://vuelo.papricacomunicacao.com.br/politica-de-entrega"><?php _e("Política de Entrega", "vuelo"); ?></a></p>
						<p><a href="http://vuelo.papricacomunicacao.com.br/politica-de-troca-e-devolucao"><?php _e("Política de Troca e Devolução", "vuelo"); ?></a></p>
					</div>
				</div>
			</div>						

			<div id="col-5" class="col-lg-4 col-md-12 col-sm-12 map">
				<div class="widgets_footer">

					<div class="textwidget hidden-xs hidden-sm hidden-md">
						<div class="row">	
						<h3>Onde nos encontrar</h3>						
							<div class="col-xs-2 pin">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="col-xs-10">
								<span><?php _e("Matriz", "vuelo"); ?></span>
								<p>Rua Professor assis Gonçalves, 111</p>
								<p>Água Verde, Curitiba - PR</p>
							</div>
						</div>
						<div class="row iframe">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.5209392511024!2d-49.28317258487081!3d-25.454274983777303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce4808e287e57%3A0x541fb0b67b2a61cc!2sR.+Prof.+Assis+Gon%C3%A7alves%2C+111+-+%C3%81gua+Verde%2C+Curitiba+-+PR%2C+80620-250!5e0!3m2!1spt-BR!2sbr!4v1490190077881" width="90%" height="185" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>

					<div class="textwidget hidden-md hidden-lg">						
						<div class="pin">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</div>
						<div class="text-pin">
							<span><?php _e("Matriz", "vuelo"); ?></span>
							<p>Rua Professor assis Gonçalves, 111</p>
							<p>Aguá Verde, Curitiba - PR</p>
						</div>						
					</div>

					<div class="textwidget hidden-xs hidden-sm hidden-lg">
						<div class="row">						
							<div class="col-xs-2 col-md-1 pin">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="col-xs-10 col-md-5">
								<span><?php _e("Administrativo", "vuelo"); ?></span>
								<p>Av. Nossa Senhora Aparecida, 1138</p>
								<p>Seminário, Curitiba – PR CEP 80.310-100</p>
							</div>
							<div class="col-xs-2 col-md-1 pin">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="col-xs-10 col-md-4">
								<span><?php _e("Fábrica", "vuelo"); ?></span>
								<p>Rua Antonio de Paula Braga, 83</p>
								<p>Almirante Tamandaré Região Metropolitana de Curitiba, PR. CEP: 83506-010</p>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div id="col-3" class="col-lg-4 col-md-3 col-sm-12 face">
				<div class="widgets_footer">
					<h3>Siga nossa página</h3>
					<div class="textwidget">
						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fprintexpressbrasil%2F%3Ffref%3Dts&tabs=timeline&width=370&height=265&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="370" height="265" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>					
					</div>
				</div>
			</div>			
			
			<div id="division" class="col-xs-12"></div>
			<div class="clearfix"></div>
			<div id="bot-footer">
				<div class="col-lg-5">
					<p>©2017 Todos os direitos reservados.</p>
				</div>
				<div class="col-lg-4">
					<p></p>
				</div>
				<div class="col-lg-3">
					<a href="http://papricacomunicacao.com.br/" target="_blank">
						<img class="footer-logo" src="<?php bloginfo("template_url"); ?>/_assets/img/logo.png">
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</footer><!-- .site-footer -->
	</div>
</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
