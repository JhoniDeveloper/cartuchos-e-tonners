<?php
	get_header();
	$category = get_term_by("name", single_cat_title("", false), "category" );
?>

	<div id="nav-bar">
		<p>
			<a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a> > 
			<a href="<?php echo home_url(); ?>/blog"><span><?php _e("BLOG", "vuelo"); ?></span></a> >
			<span><?php single_cat_title(); ?></span>
		</p>
	</div>
	<section id="category">
		<h1 class="title-page"><?php single_cat_title(); ?></h1>
		<div class="posts">
			<div class="row">
				<div class="col-md-8">
					<?php
						if(have_posts()){
						    while (have_posts()) {
								the_post();
								get_template_part("inc/loop", "post");
							}
						} else {
							get_template_part( 'content', 'none' );
						}
				 	?>
				 	<?php if ($wp_query->max_num_pages > 1): ?>
						<div class="load-more">
							<div class="loader">
								<div class="stick1"></div>
								<div class="stick2"></div>
								<div class="stick3"></div>
								<div class="stick4"></div>
							</div>
							<div class="col-md-7 col-md-offset-5">
								<button class="btn-lg btn-block hvr-wobble-horizontal load-more-button" data-type="category" data-category="<?php echo $category->term_id; ?>" data-paged="2" data-max-paged="<?php echo $wp_query->max_num_pages; ?>">
									<?php _e("CARREGAR MAIS PUBLICAÇÕES", "vuelo"); ?>
								</button>
							</div>
						</div>
					<?php endif ?>
					<?php wp_reset_query(); ?>
			 	</div>
		 		<div class="col-md-4">
			 		<?php get_sidebar("blog"); ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>