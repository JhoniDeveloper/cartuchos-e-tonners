<section class="Produtos mais-vendidos">
    <h2 class="title">Mais Vendidos</h2>
    <div class="container">
        <div class="row"> 
        <?php 
            $aux = 0;
            $params = array(
                'post_type' => 'product',
                'posts_per_page' => 4,
                'order' => 'ASC',
                'product_cat' => 'mais-vendidos'
            );

            $wc_query = new WP_Query($params);

            if ($wc_query->have_posts()):
                while ($wc_query->have_posts()):
                    $wc_query->the_post();
                    $aux++;
                    if (has_post_thumbnail()) {
                        $thumbnail = get_the_post_thumbnail(get_the_ID() , 'full');
                        $thumbnail_url = $thumbnail_data[0];
                    }
                    if($aux == 0){echo "<div class='row'>";}else if($aux % 5 == 0){echo "</div><div class='row'>";}
        ?>            
                        <div class="col-lg-3 <?php if($aux % 4 == 0){echo 'border-right-none'; } ?>">
                            <figure>    
                                <a href="<?php the_permalink(); ?>">                                
                                    <?php echo $thumbnail; ?>                                
                                </a>
                            </figure>
                            <div class="info">
                                <a href="<?php the_permalink(); ?>">
                                    <h4><?php the_title(); ?></h4>
                                </a>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="btn-lg btn-block hvr-wobble-horizontal"> SAIBA MAIS </a>
                            </div>
                        </div>                          
                    <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>  
        </div>                      
    </div>
</section>