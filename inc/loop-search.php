<aside class="post post-default">
	<div class="row">
	 	<div class="col-sm-5">
			<div class="image">
				<a href="<?php the_permalink() ?>">
					<img src="<?php bloginfo("template_url"); ?>/_assets/img/default-blog.png" alt="<?php the_title(); ?>">
				</a>
			</div>
		</div>
		<div class="col-sm-7">
			<div class="content">
				<nav class="social-share">
					<?php if (wp_is_mobile()): ?>
						<a href="whatsapp://send?text=<?php echo urlencode(get_permalink($post->ID)); ?>" target="_blank" class="fa fa-whatsapp"></a>
					<?php endif ?>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>" target="blank" class="fa fa-facebook"></a>
					<a href="https://twitter.com/home?status=<?php echo urlencode($post->post_title." - ".get_permalink($post->ID)); ?>" target="blank" class="fa fa-twitter"></a>
					<?php if (has_post_thumbnail()): ?>
					<a href="<?php
						$img = wp_get_attachment_image_src(get_post_thumbnail_id(), "full");
						echo 'https://pinterest.com/pin/create/button/?url='.urlencode(get_permalink($post->ID)).'&media='.urlencode($img[0]);
					?>" target="_blank" class="fa fa-pinterest"></a>	
					<?php endif ?>
					<a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink($post->ID)); ?>" target="blank" class="fa fa-google"></a>
					<a href="mailto:?&body=<?php echo $post->post_title." - ".get_permalink($post->ID); ?>" target="blank" class="fa fa-envelope"></a>
				</nav>

				<h3><?php the_title(); ?></h3>
				<?php the_excerpt(); ?>
				<a href="<?php the_permalink() ?>" class="btn-lg btn-block hvr-wobble-horizontal"><?php _e("SAIBA MAIS", "vuelo"); ?></a>
			</div>
		</div>
	</div> 
</aside>