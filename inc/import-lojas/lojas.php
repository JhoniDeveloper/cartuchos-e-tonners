<?php 

	include '../../../../../wp-load.php';
	
	$xml = simplexml_load_file("membracel-06-12-16.xml");

	if ($xml->row) {
		
		foreach ($xml->row as $loja) {

			$data = array(
				'post_title' => $loja->field[1],
				'post_content' => wp_strip_all_tags($loja->field[2]),
				'post_status' => 'publish',
				'post_type' => 'distribuidores'
			);

			$insert_id = wp_insert_post($data);

			switch ($loja->field[4]) {

				case 'ACRE':
					$uf = 'AC';
					break;
				case 'ALAGOAS':
					$uf = 'AL';
					break;
				case 'AMAPÁ':
					$uf = 'AP';
					break;
				case 'AMAZONAS':
					$uf = 'AM';
					break;
				case 'BAHIA':
					$uf = 'BA';
					break;
				case 'CEARÁ':
					$uf = 'CE';
					break;
				case 'DISTRITO FEDERAL':
					$uf = 'DF';
					break;
				case 'ESTPÍRITO SANTO':
					$uf = 'ES';
					break;
				case 'GOIÁS':
					$uf = 'GO';
					break;
				case 'MARANHÃO':
					$uf = 'MA';
					break;
				case 'MATO GROSSO':
					$uf = 'MT';
					break;
				case 'MATO GROSSO DO SUL':
					$uf = 'MS';
					break;
				case 'MINAS GERAIS':
					$uf = 'MG';
					break;
				case 'PARÁ':
					$uf = 'PA';
					break;
				case 'PARAÍBA':
					$uf = 'PB';
					break;
				case 'PARANÁ':
					$uf = 'PR';
					break;
				case 'PERNAMBUCO':
					$uf = 'PE';
					break;
				case 'PIAUÍ':
					$uf = 'PI';
					break;
				case 'RIO DE JANEIO':
					$uf = 'RJ';
					break;
				case 'RIO GRANDE DO NORTE':
					$uf = 'RN';
					break;
				case 'RIO GRANDE DO SUL':
					$uf = 'RS';
					break;
				case 'RONDÔNIA':
					$uf = 'RO';
					break;
				case 'RORAIMA':
					$uf = 'RR';
					break;
				case 'SANTA CATARINA':
					$uf = 'SC';
					break;
				case 'SÃO PAULO':
					$uf = 'SP';
					break;
				case 'SERGIPE':
					$uf = 'SE';
					break;
				case 'TOCANTINS':
					$uf = 'TO';
					break;
				default:
					$uf = "";
					break;
			}

			update_post_meta( $insert_id, 'cidade_distribuidor', ucwords(strtolower($loja->field[3])) );
			update_post_meta( $insert_id, 'estado_distribuidor', $uf );

		}

	}
	exit();

	query_posts(
		array(
			'post_type' => "loja",
			'posts_per_page' => -1
		)
	);
?>
<table border="1" cellpadding="10" cellspacing="0">
	<tr>
		<th>Nome</th>
		<td>Conteúdo</td>
		<td>Cidade</td>
		<td>Estado</td>
	</tr>
	<?php
		while (have_posts()) {
		the_post();

		$city = "";
		$state = "";

		if ($post->post_title != "LOJAS VIRTUAIS"):

		?>
		<tr>
			<td><?php the_title(); ?></td>
			<td><?php the_content(); ?></td>
			<td><?php
				if (get_the_terms( $post->ID, "Cidade" )) {
					foreach (get_the_terms( $post->ID, "Cidade" ) as $cidade) {
						if ($cidade->parent) {
							echo $cidade->name;
							$city = $cidade->name;
						}
					}
				};
			?> </td>
			<td><?php
				if (get_the_terms( $post->ID, "Cidade" )) {
					foreach (get_the_terms( $post->ID, "Cidade" ) as $cidade) {
						if ($cidade->parent || $cidade->parent == 0) {
							$parent = get_term_by("term_id", $cidade->parent, "Cidade" );
							echo $parent->name;
							$state = $parent->name;
						}
					}
				};
			?> </td>
		</tr>
		<?php

		// $wpdb->insert(
		// 	"distribuidores",
		// 	array(
		// 		"title" => $post->post_title,
		// 		"description" => wpautop($post->post_content),
		// 		"cidade" => $city,
		// 		"estado" => $state
		// 	)
		// );

		endif;
	}
	?>
</table>


