<div class="perguntas-frequentes">
	<h2><?php _e("Perguntas Frequentes", "vuelo"); ?></h2>
	<?php
        $terms = get_terms( 'taxonomy_faq' );
    ?>
	<div class="col-categorias">
	  	<ul class="nav nav-tabs" role="tablist">
	  		<?php 
		  		$count = 1;
		  		foreach ( $terms as $term ) { 
			?>
	        <li role="presentation">
	        	<span class="term-text"><?php echo $term->name; ?></span>
		        <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="<?php echo $term->slug; ?>">
						<div class="panel-group" id="accordion-mobile" role="tablist" aria-multiselectable="true">
						<?php 
							$args = array(
		                        'post_type' => 'faq',
		                        'posts_per_page' => 1000,
		                        'tax_query' => array(
									array(
										'taxonomy' => 'taxonomy_faq',
										'field'    => 'slug',
										'terms'    => $term->slug
									)
								)
		                    );
			                query_posts($args);
			                if(have_posts()): while(have_posts()): the_post();
						?>
							<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="heading-<?php echo $term->slug; ?>">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion-mobile" href="#collapse-<?php echo $post->ID; ?>-mobile" aria-expanded="true" aria-controls="collapse-<?php echo $post->ID; ?>">
							          <?php the_title(); ?>
							        </a>
							      </h4>
							    </div>
							    <div id="collapse-<?php echo $post->ID; ?>-mobile" class="panel-collapse collapse <?php if($count === 1){ echo 'in'; } ?>" role="tabpanel" aria-labelledby="heading-<?php echo $count; ?>">
							      <div class="panel-body">
							        <?php the_content(); ?>
							      </div>
							    </div>
							</div>
						  <?php $count++; ?>
						  <?php endwhile; endif; ?>
						</div><!-- /panel-group -->
				    </div><!-- /tab-panel -->
				</div><!-- /tab-content -->
	        </li>
	        <?php } ?>
	  	</ul>
	</div><!-- /col-categorias -->
</div><!-- /perguntas-frequentes -->