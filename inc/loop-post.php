<?php
	if (has_post_thumbnail()) {
		$thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'blog-mini' );
		$thumbnail_url = $thumbnail_data[0];
	}
?>
<aside class="post">
	<div class="row">
	 	<div class="col-sm-5">
			<div class="image">
				<a href="<?php the_permalink() ?>" class="open-post">
					<?php if ($thumbnail_url): ?>
						<img src="<?php echo $thumbnail_url; ?>" alt="">
					<?php else: ?>
						<img src="<?php bloginfo("template_url"); ?>/_assets/img/default-blog.png" alt="<?php the_title(); ?>">
					<?php endif ?>
				</a>
			</div>
		</div>
		<div class="col-sm-7">
			<div class="content">
				<nav class="social-share">
					<?php if (wp_is_mobile()): ?>
						<a href="whatsapp://send?text=<?php echo urlencode(get_permalink($post->ID)); ?>" target="_blank" class="fa fa-whatsapp"></a>
					<?php endif ?>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>" target="blank" class="fa fa-facebook"></a>
					<a href="mailto:?&body=<?php echo $post->post_title." - ".get_permalink($post->ID); ?>" target="blank" class="fa fa-envelope"></a>
				</nav>
				
				<?php if (get_the_category()): ?>
					<span><?php the_category(" - "); ?></span>	
				<?php endif ?>

				<h3><?php the_title(); ?></h3>
				<a href="<?php the_permalink() ?>" class="btn-lg btn-block hvr-wobble-horizontal open-post"><?php _e("SAIBA MAIS", "vuelo"); ?></a>
			</div>
		</div>
	</div> 
</aside>