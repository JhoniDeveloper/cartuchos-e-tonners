<!DOCTYPE html>
<html>
<head>
	<title><?php echo wp_title('|', true, 'right') . get_bloginfo('name'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url')?>/_assets_/img/favicons/favicon.png" type="image/x-icon"/>
	
	<title><?php echo get_bloginfo("name") . wp_title(" | "); ?></title>

	<script>var theme_directory = "<?php echo get_template_directory_uri() ?>";</script>

	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">

	<?php wp_head(); ?>

	<script> 
		var $buoop = {vs:{i:8,f:-8,o:-8,s:7,c:-8},api:4}; 
		function $buo_f(){ 
		 var e = document.createElement("script"); 
		 e.src = "//browser-update.org/update.min.js"; 
		 document.body.appendChild(e);
		};
		try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
		catch(e){window.attachEvent("onload", $buo_f)}
	</script>	

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/_assets/css/animate.css">
</head>

<body id="<?php if (is_home()) { echo "wp-vuelo"; } else { the_slug(); } ?>" <?php body_class(); ?>>
	<div id="fb-root"></div>
	<div class="identify-home-js" style='visibility: hidden; position: absolute;'><?php if(is_home()) { echo "homejs"; } ?></div>
	<div id="all">
		<div class="cont" class="container">
			<header>
				<div class="menu_header">
					<div class="div-border"></div>
					<?php include 'produtos-header.php'; ?>
					<div class="menu_superior">
						<div class="container">
							<div class="row">
								<div class="col-sm-3">
									<div class="opcoes_superior">						
										<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
											<span>
												Faça login ou cadastre - se
											</span>
										</a>										
									</div>								
								</div>
								<div class="col-sm-6">
									<div>
										<i class="fa fa-whatsapp" aria-hidden="true"></i>
										<p>41 | 9 0000 - 0000</p><p><a href="">2° Via boleto</a></p>
									</div>
								</div>
								<div class="col-sm-3"></div>
							</div>
						</div>
					</div>

					<div class="menu_normal">
						<div id="menu_inferior">
							<div class="container">
								<div class="row">
									<div class="col-sm-2 logo_menu">
										<div class="menu_mobile_div">
											<button class="hamburger">&#9776;</button>
											<button class="cross">&#735;</button>
										</div>

										<div class="logo">
											<div class="align_logo">
												<a href="<?php echo get_bloginfo('url'); ?>">
													<img src="<?php echo get_bloginfo('template_url').'/_assets/img/logo-branca.png'; ?>">
												</a>	
											</div>
										</div>							
									</div>
									<div class="col-sm-8 busca">
										<div class="align_opcoes">
											<div class="o coes_inferior">
												<form class="search-box" action="<?php bloginfo('url'); ?>" method="GET">
													<input type="text" name="s" class="form-control" placeholder="<?php _e("O que você procura?", "vuelo"); ?>">
													<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
												</form>
											</div>				
										</div>								
									</div>
									<div class="col-sm-2 icons_ecommerce">
										<div class="menu_icon">                                 
											<span class="menu-icon">
												<a href="<?php echo get_bloginfo('ulr')?>/wishlist/" class="favorite">
														<i class="fa fa-star" aria-hidden="true"></i>
													</a>
											</span>							
										     <a href="<?php echo get_bloginfo('url'); ?>/carrinho/" class="modal-cart-open">                                 
												<span class="menu-icon">
													<i class="fa fa-shopping-cart" aria-hidden="true">
													<?php if(WC()->cart->get_cart_contents_count()): ?>
														<div class="cart_itens_total">
															<?php echo WC()->cart->get_cart_contents_count(); ?>		
														</div>		
													<?php endif; ?>
													</i>
												</span>
											 </a>										 
										</div>
										<?php
											global $woocommerce;
											$items = $woocommerce->cart->get_cart();	
										?>
										<?php if ($items): ?>
										<div class="modal-cart">
											<button class="modal-cart-close">
												<i class="fa fa-remove"></i>
											</button>
											<ul class="modal-cart-items">
											<?php foreach($items as $item => $values): ?>
												<?php
													$produto = wc_get_product( $values['product_id'] );
												?>
												<li>
													<div class="cart-item-image">
														<?php echo $produto->get_image("full"); ?>
													</div>
													<div class="cart-item-description">
														<em class="cart-modal-title"><?php echo $produto->get_title(); ?></em>
														<span class="cart-modal-price">
															<?php
																echo $values["quantity"] ."x ". WC()->cart->get_product_subtotal( $produto, $values["quantity"]);
															?>
														</span>
													</div>
												</li>
											<?php endforeach ?>
											</ul>
											<ul class="modal-cart-total">
												<li>
													Subtotal: <span><?php echo wc_cart_totals_subtotal_html(); ?></span>
												</li>
											</ul>
											<nav>
												<a href="<?php echo wc_get_cart_url(); ?>" class="checkout-button button alt wc-forward wc-cart"><?php _e("Ver Carrinho", "vuelo"); ?></a>
												<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
											</nav>
										</div>
										<?php endif ?>								
									</div>
								</div>						
							</div>
						</div>
					</div>

					<div class="header-menu">
						<div class="relative">
							<div class="custom-menu">
								<div class="container">
									<div class="button">
										<button type="button" class="button-responsive">			
											<?php for ($i=0; $i < 3; $i++) { ?>
													<svg version="1.1" id="Camas:xlink="http://www.w3.org/1999/xlink" x="0pda_1" xmlns="http://www.w3.org/2000/svg" xmlnx" y="0px"
														 width="30px" height="5px" viewBox="0 0 30 5" enable-background="new 0 0 30 5" xml:space="preserve">
														<path fill="#ffffff" d="M30,3.846C30,4.483,29.487,5,28.854,5H1.146C0.513,5,0,4.483,0,3.846V1.154C0,0.517,0.513,0,1.146,0h27.707
															C29.487,0,30,0.517,30,1.154V3.846z"/>
													</svg>			
											<?php } ?>
											<p class="compre">COMPRE POR MARCA</p>
										</button>					
									</div>						
									<div class="row">
										<div class="col-sm-12 sub">
											<nav>
												<ul class="principal">
													<li>
														<a href="#"><b>Compre por marca</b></a>
													</li>
													<li>
														<a href="#">Brother</a>
														<nav class="fadeInDown filho">
															<ul>
																<li>
																	<a href="#">Cartuchos</a>
						
																<li>
																	<a href="#">Tonners</a>
																</li>
																<li>
																	<a href="#">Impressoras</a>
																</li>
																<li>
																	<a href="#">Peças</a>
																</li>
															</ul>									
														</nav>										
													</li>
													<li>
														<a href="#">Cannon</a>
														<nav class="fadeInDown filho">
															<ul>

															</ul>
														</nav>										
													</li>
													<li>
														<a href="#">Epson</a>
														<nav class="fadeInDown filho">
															<ul>

															</ul>
														</nav>										
													</li>
													<li>
														<a href="#">Hp</a>
														<nav class="fadeInDown filho">
															<ul>

															</ul>
														</nav>										
													</li>
													<li>
														<a href="#">Lexmarc</a>
														<nav class="fadeInDown filho">
															<ul>

															</ul>
														</nav>										
													</li>
													<li>
														<a href="#">Xerox</a>
														<nav class="fadeInDown filho">
															<ul>

															</ul>
														</nav>										
													</li>
												</ul>
											</nav>
										</div>
									</div>							
								</div>
							</div>
						</div>
					</div>

					<div class="face-action display_social">
						<a href="#" tagert="_blank">
							<div class="row">
								<div class="col-sm-4">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</div>
							</div>							
						</a>
					</div>

					<div class="whats-action display_social">
						<div class="row">
							<div class="first">
							<p>41 | 9 0000 - 0000</p>
							</div>						
							<div class="last">
								<i class="fa fa-whatsapp" aria-hidden="true"></i>
							</div>
						</div>
					</div>					
					
					<div class="mobile_menu">
						<div id="menu_inferior">
							<div class="menu_mobile_div">
								<button class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></button>
								<button class="cross"><i class="fa fa-times" aria-hidden="true"></i></button>
							</div>
							<div class="logo">
								<div class="align_logo">
									<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php echo file_get_contents(get_template_directory_uri().'/_assets/img/logo-branca.png'); ?>                            
									</a>
								</div>
							</div>

							<div class="menu_icon_mobile">
							   	<a href="<?php echo wc_get_cart_url(); ?>">
							   		<span class="menu-icon">
							   			<i class="fa fa-shopping-cart" aria-hidden="true">
							   			<?php if(WC()->cart->get_cart_contents_count()): ?>
								 			<div class="cart_itens_total">
								 				<?php echo WC()->cart->get_cart_contents_count(); ?>		
								 			</div>		
								 		<?php endif; ?>
							   			</i>
							   		</span>
							   	</a>
							</div>
							<div class="opcoes_inferior">
								<form id="search-box" class="search-box-mobile">
									<input type="text" name="s" class="form-control" placeholder="<?php _e("O que você procura?", "vuelo"); ?>">
									<button type="submit" id="search-open"><i class="fa fa-search" aria-hidden="true"></i></button>
								</form>
								<!-- Menu Mobile -->
								<div class="menu-vuelo-menu-container">
									<ul id="vuelo-menu" class="nav navbar-nav text-center">
										<li class="sobre menu-item">
											<a href="<?php echo site_url(); ?>/sobre/"><?php _e("Sobre", "vuelo"); ?></a>
										</li>
										<li class="produtos-header menu-item menu-item-has-children">
											<a href="#"><?php _e("Produtos", "vuelo"); ?></a>
											<ul class="sub-menu" style="display: none;">
												<li class="menu-item">
													<a href="<?php echo site_url(); ?>/produto/membracel-membrana-regeneradora-porosa/">
														Membracel <span><?php _e("Membrana Regeneradora Porosa", "vuelo"); ?></span>
													</a>
												</li>
												<li class="menu-item">
													<a href="<?php echo site_url(); ?>/produto/membracel-membrana-regeneradora-sem-poros/">
														Membracel <span><?php _e("Membrana Regeneradora Sem Poros", "vuelo"); ?></span>
													</a>
												</li>
												<li class="menu-item">
													<a href="<?php echo site_url(); ?>/produto/gelificador-para-bolsas-de-estomia/">
														<?php _e("Gelificador", "vuelo"); ?> <span><?php _e("Para bolsas de estomia", "vuelo"); ?></span>
													</a>
												</li>
												<li class="menu-item">
													<a href="<?php echo site_url(); ?>/produto/spray-de-barreira-protetor-cutaneo/">
														<?php _e("Spray de Barreira", "vuelo"); ?> <span><?php _e("Protetor Cutâneo", "vuelo"); ?></span>
													</a>
												</li>
											</ul>
										</li>
										<li class="distribuidores menu-item">
											<a href="<?php echo site_url(); ?>/distribuidores-vuelo/">
												<?php _e("Distribuidores", "vuelo"); ?>
											</a>
										</li>
										<li class="blog menu-item">
											<a href="<?php echo site_url(); ?>/blog/">
												<?php _e("Blog", "vuelo"); ?>
											</a>
										</li>
										<li class="contato menu-item">
											<a href="<?php echo site_url(); ?>/contato/">
												<?php _e("Contato", "vuelo"); ?>
											</a>
										</li>
									</ul>
								</div>
								<div class="menu_icon">
								   <span id="search" class="menu-icon">
								   		<i class="fa fa-star" aria-hidden="true"></i>
								   </span>

								   <span class="menu-icon">
								   	<i class="fa fa-shopping-cart" aria-hidden="true"></i>
								   </span> 
								</div>
							</div>
						</div>
					</div>
				</div>			
			</header>			
			<div id="content">
				<div class="scrolTop">
					<div style="position: relative;">
						<i class="fa fa-angle-up" aria-hidden="true"></i>
					</div>
				</div>