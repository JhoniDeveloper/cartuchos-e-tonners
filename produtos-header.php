<div id="produtos-header">
	<div id="title" class="col-lg-2">
		<h2>PRODUTOS</h2>
	</div>
	<?php 
        $aux = 0;
        $params = array(
            'post_type' => 'product',
            'posts_per_page' => 4,                   
        );

        $wc_query = new WP_Query($params);

        if ($wc_query->have_posts()):
            while ($wc_query->have_posts()):
                $wc_query->the_post();

                if (has_post_thumbnail()) {
                    $thumbnail = get_the_post_thumbnail(get_the_ID() , 'full');
                    $thumbnail_url = $thumbnail_data[0];
                }
    ?>
	<div class="col-lg-2 content">
		<a href="<?php the_permalink(); ?>"><?php echo $thumbnail; ?></a>
		<div class="info">
			<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
		</div>
	</div>
            <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>
	<div id="close" class="col-lg-2">
		<div class="outer">
		  	<div class="inner">
		    	<label><?php _e("Fechar", "vuelo"); ?></label>
		  	</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>