<?php get_header(); ?>

<section class="custom-slider">
    <?php  echo do_shortcode("[rev_slider home]"); ?>
    <i class="fa fa-angle-double-down" aria-hidden="true"></i>
</section>    

<h2 class="title">FRETE GRÁTIS</h2>

<section class="Destaques_Home">
    <div class="row">
        <div class="col-sm-3">
        <a href="#">
            <div class="Descricao">
                <div>
                    <h3>Cartuchos de Tinta</h3>
                </div>
            </div>        
        </a>
            <img src="<?php echo get_bloginfo('template_url')?>/_assets/img/categorias/cartucho-de-toner.jpg">
        </div>
        <div class="col-sm-3">
        <a href="#">
            <div class="Descricao">
                <div>
                    <h3>Cartuchos de Tinta</h3>
                </div>
            </div>        
        </a>
            <img src="<?php echo get_bloginfo('template_url')?>/_assets/img/categorias/cartucho-de-toner.jpg">
        </div>
        <div class="col-sm-3">
        <a href="#">
            <div class="Descricao">
                <div>
                    <h3>Cartuchos de Tinta</h3>
                </div>
            </div>        
        </a>
            <img src="<?php echo get_bloginfo('template_url')?>/_assets/img/categorias/cartucho-de-toner.jpg">
        </div>
        <div class="col-sm-3">
        <a href="#">
            <div class="Descricao">
                <div>
                    <h3>Cartuchos de Tinta</h3>
                </div>
            </div>        
        </a>
            <img src="<?php echo get_bloginfo('template_url')?>/_assets/img/categorias/cartucho-de-toner.jpg">
        </div>               
    </div>
</section>

<?php include("inc/produtos-lancamento.php"); ?>
<?php include("inc/produtos-mais-vendidos.php"); ?>
<?php include("inc/produtos-mais-visitados.php"); ?>

<section class="newsletter">
    <div class="container">
        <div class="row">            
            <div class="col-sm-2"></div>
            <div class="col-sm-4">
                <h2>Receba nossas novidades</h2>              
            </div>
            <div class="col-sm-4">
                <div class="news">
                    <div class="row">
                        <div class="col-sm-11"><input type="text"></div>
                        <div class="col-sm-1"><i class="fa fa-paper-plane" aria-hidden="true"></i></div>
                    </div>                    
                </div>                  
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
