jQuery(document).ready(function($) {

	jQuery(".custom-slider").mouseover(function(){
		jQuery(".custom-slider i").addClass("bounce animated");
		
		setTimeout(function(){
			jQuery(".custom-slider i").removeClass("bounce animated");
		}, 1000);
	});

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});

	$('#wp-vuelo #forms .forms').matchHeight();
	$('.altura-box').matchHeight();
	$('.altura-profissional').matchHeight();
	$('.produtos-highlight dl').matchHeight();
	$('.produtos-indicacoes article').matchHeight();

	$(window).resize(function(event) {
		$('#wp-vuelo #forms .forms').matchHeight();
		$('.altura-box').matchHeight();
		$('.altura-profissional').matchHeight();
		$('.produtos-highlight dl').matchHeight();
		$('.produtos-indicacoes article').matchHeight();
	});

	$.fn.extend({
		animateCss: function (animationName) {
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			this.addClass('animated ' + animationName).one(animationEnd, function() {
				$(this).removeClass('animated ' + animationName);
			});
		}
	});

	$("#owl-slider").owlCarousel({
		navigation : true, 
		slideSpeed : 700,
		paginationSpeed : 700,
		singleItem:true,
		navigationText : false,
		afterAction : afterAction,
		autoPlay : 8000,
		addClassActive: true,
	    afterMove: previousslide,
	    beforeMove: nextslide
	});

	// First Slide
	$("#owl-slider .owl-item.active p").addClass('animated bounceInUp');
	$("#owl-slider .owl-item.active .btn-banner").addClass('animated bounceInLeft');
	 
	// Other Slides
	function previousslide() {
	    $("#owl-slider .owl-item.active p").addClass('animated bounceInUp');
	    $("#owl-slider .owl-item.active .btn-banner").addClass('animated bounceInLeft');
	}
	function nextslide() {
	     $("#owl-slider .owl-item p").removeClass('animated bounceInUp');
	     $("#owl-slider .owl-item .btn-banner").removeClass('animated bounceInLeft');
	}

	$("#owl-slider-mobile").owlCarousel({
		navigation : false,
	    slideSpeed : 300,
	    paginationSpeed : 400,
	    singleItem:true,
	    navigationText : false,
	    autoPlay : 4000,
	    pagination : false
	});


	var status = $("#owlStatus");
	
	function afterAction(){
		$("#owl-slider").attr("i",this.owl.owlItems.length);
	}


	$('form .wysija-checkbox-paragraph .wysija-checkbox').attr("type", "radio");

	$(window).trigger('resize').trigger('scroll');

	var wow = new WOW ({
		boxClass:     'wow',      // animated element css class (default is wow)
		animateClass: 'animated', // animation css class (default is animated)
		offset:       0,          // distance to the element when triggering the animation (default is 0)
		mobile:       true,       // trigger animations on mobile devices (default is true)
		live:         true,       // act on asynchronously loaded content (default is true)
		scrollContainer: null // optional scroll container selector, otherwise use window
	});
	wow.init();

	jQuery(".open-search-header").click(function(event) {
		var $menu = $(this).parents(".opcoes_inferior");
		$menu.find('.search-box').slideDown();
		$menu.find('input[name="s"]').focus();
		$menu.find('input[name="s"]').focusout(function(event) {
			$menu.find('.search-box').slideUp();
		});
	});

	/* -- Login -- */
		jQuery(".woocommerce-Button.button").addClass("hvr-wobble-horizontal");	
		jQuery(".woocommerce-LostPassword").addClass("hvr-wobble-horizontal");	
		jQuery(".woocommerce-MyAccount-navigation-link a").addClass("hvr-wobble-horizontal");	
	/* -- Login -- */

	/*
	*
	* Produtos
	*
	*/
	jQuery(".produtos-description-accordion dl dt").on('click', function(event) {
		event.preventDefault();
		var $dl = $(this).parent().find('dd');
		$dl.slideToggle();
		$(this).toggleClass('active');
	});

	jQuery(".show-more-button").on('click', function(event) {
		event.preventDefault();
		jQuery(".show-more-content").slideToggle();
		$(this).find('span').toggleClass('fa-angle-up fa-angle-down');
	});

	$(".gallery-slider").owlCarousel({
		navigation: true,
		slideSpeed: 300,
		paginationSpeed : 400,
		singleItem: true,
		navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		autoHeight : true,
		pagination: false
	});


	$('.wysija-submit-field').addClass('hvr-wobble-horizontal');
	$('.nav-highlight ol li a').addClass('hvr-wobble-horizontal');

	/* ---------------- page-contato.php ------------*/
	jQuery("#mailing-form").submit(function(event) {
		var button = jQuery("#loadButton").html();
		jQuery("#loadButton").html('<p>AGUARDE</p> <p><i class="fa fa-circle-o-notch fa-spin"></i></p>');
		jQuery.ajax({
			url: vuelo.template+'/inc/form-contato.php',
			type: 'POST',
			data: jQuery("#mailing-form").serialize()
		}).always(function(result) {
			if (result == "Enviou") {
				jQuery("#loadButton").html(button).removeAttr('disabled');
				jQuery(".alert-success").show('slow', function() {
					setTimeout(function() {
						jQuery(".alert-success").hide('fast', function() {
							jQuery(this).css("display", "none");
						});
						jQuery("#mailing-form input").val("");
						jQuery("#mailing-form textarea").val("");
					}, 3000);
				});	 	    			
			} else {		
				jQuery("#loadButton").html(button);
				jQuery(".alert-unsucces").show('fast', function() {
					setTimeout(function() {
						jQuery(".alert-unsucces").hide('fast', function() {
							jQuery(this).css("display", "none");
						});
					}, 3000);
				});	    				 					    					    				    		
			}
		});	    			    
		return false;
	});	  

	function validateEmail(email) {
	  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}

	jQuery(".alert-unsucces").hide();
	jQuery('#email_contato').blur(function(){
		var email = jQuery(this).val();
		if(!validateEmail(email))
		{
			jQuery(".alert-unsucces").show('slow', function() 
			{
				jQuery(this).val("");
				setTimeout(function() {
					jQuery(".alert-unsucces").hide();
				}, 3000);	
			});								
		}
	});	
	/* ---------------- page-contato.php ------------*/	 

	if ($(window).width() < 991) {
	   $("#sobre #quadros #essencia").removeClass("wow slideInLeft");
	   $("#sobre #quadros #missao").removeClass("wow slideInRight");
	}
});