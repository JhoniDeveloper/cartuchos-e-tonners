jQuery(document).ready(function($) 
{

	var width = jQuery( window ).width();
	//console.log(width);

	var big = 8;
	var small = 6;
	var delay = 150;
	var file = theme_directory + '/_assets/img/animacao-home/animation1.svg';
	var opts,
	balls,
	rotation,
	lastId,
	text_anime,
	text_title;

	$.getJSON(vuelo.template+'/inc/traducao_home_animate.php', function(data) {
		/*optional stuff to do after success */
		//console.log(data);	

		for(var i=0; i<big*small; i++) {
			$('.pivot').append('<div data-id="'+i+'" class="ball-container"><div class="ball"><div class="dot"><span></span></div></div></div>');
		}

		opts = $('.pivot').data();
		balls = $('.pivot').find('.ball-container');
		rotation = (360 / balls.length);
		lastId = 0;

		$.fn.extend({
		    animateCss: function (animationName) {
		        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		        this.addClass('animated ' + animationName).one(animationEnd, function() {
		            $(this).removeClass('animated ' + animationName);
		        });
		    }
		});

		$(balls).each(function(i, value) {
			if(!(i%small)) {
				$(balls[i]).addClass('big');
			}
			$(balls[i]).css({
			'transform': 'rotate(' + (rotation * i - 92) + 'deg) translate('+opts.radius+'px)',
			'width': opts.size,
			'height': opts.size,
			});
		});

		function conteudoAnimate(id)		
		{
			if (id == 0) {
				file = theme_directory + '/_assets/img/animacao-home/animation1.svg';
				text_title = data.texto_1.title;
				text_anime = data.texto_1.cont;					
			} else if (id == 6) {
				file = theme_directory + '/_assets/img/animacao-home/animation2.svg';
				text_title = data.texto_2.title;
				text_anime = data.texto_2.cont;					
			} else if (id == 12) {
				file = theme_directory + '/_assets/img/animacao-home/animation-certa3.svg';
				text_title = data.texto_3.title;
				text_anime = data.texto_3.cont;					
			} else if (id == 18) {
				file = theme_directory + '/_assets/img/animacao-home/animation4.svg';
				text_title = data.texto_4.title;
				text_anime = data.texto_4.cont;					
			} else if (id == 24) {
				file = theme_directory + '/_assets/img/animacao-home/animation5.svg';
				text_title = data.texto_5.title;
				text_anime = data.texto_5.cont;					
			} else if (id == 30) {
				file = theme_directory + '/_assets/img/animacao-home/animation7.svg';
				text_title = data.texto_7.title;
				text_anime = data.texto_6.cont;	
			} else if (id == 36) {
				file = theme_directory + '/_assets/img/animacao-home/animation6.svg';
				text_title = data.texto_6.title;
				text_anime = data.texto_7.cont;						
			} else if (id == 42) {		
				file = theme_directory + '/_assets/img/animacao-home/animation8.svg';
				text_title = data.texto_8.title;
				text_anime = data.texto_8.cont;					
			}
		
			$( "#my-svg" ).remove();

			new Vivus('anime', {
			    type: 'delayed',
			    duration: 100,
			    file: file,
			    animTimingFunction: Vivus.EASE
			});

			text_anime = text_anime.substr(0, 327);
			setTimeout(function()
			{		
				jQuery(".title_anime").text(text_title);
				jQuery(".cont").text(text_anime);					
			}, 500);

			jQuery(".align_texto_animate").fadeOut("slow");
			jQuery(".align_texto_animate").fadeIn("slow");
		}

		$(balls[0]).find('.ball').addClass('active');

		$(balls).filter('.big').click(function() 
		{
			var id = $(this).data('id');			
				
			if (id > lastId) { // INDO
				conteudoAnimate(id);
				animation_next(id);
				lastId = id;
			} else { // VOLTANDO
				conteudoAnimate(id);
				animation_prev(id);
				lastId = id;
			}
		});

		text_title = data.texto_1.title;
		text_anime = data.texto_1.cont;	

		text_anime = text_anime.substr(0, 327);
		
		jQuery(".title_anime").text(text_title);
		jQuery(".cont").text(text_anime);	

		jQuery(".ajuda_align.tres").hide();
		jQuery(".navegar_animate").hide();
		

		setTimeout(function()
		{		
			jQuery(".ajuda_align.tres").fadeIn('500', function() {
				jQuery(".ajuda_align.tres").show();									
			});	

			jQuery(".ajuda_align.tres .navegar_animate").fadeIn('750', function() {
				jQuery(".ajuda_align.tres .navegar_animate").show();				
			});
		}, 350);	
	});//Fim json

	// Seta Direita
	var temp = 0;
	jQuery(".icons-animate .last").click(function(event) {
		if(lastId == 0){temp = 6;} else if(lastId == 42){temp = 0;} else {temp = lastId+6;}
		$('div[data-id="'+temp+'"]').click();
	});
	// Seta Esquerda
	jQuery(".icons-animate .first").click(function(event) {
		if(lastId == 0){temp = 42;} else {temp = lastId-6;}
		$('div[data-id="'+temp+'"]').click();
	});

	function animation_next(id) {
		for (var i=lastId, j=0; i <= id; i++, j++) {
			$(balls[i]).find('.ball').delay(j*delay).animate({
				opacity: 1,
			}, 100, function() {
				$(this).animateCss('zoomIn');
				$('.ball').removeClass('active');
				$(this).addClass('active');
			});
		}
	}
	function animation_prev(id) {
		for (var i=lastId -1, j=0; i >= id ; i--, j++) {
			$(balls[i]).find('.ball').delay(j*delay).animate({
				opacity: 0.5,
			}, 100, function() {
				$(this).animateCss('zoomIn');
				$('.ball').removeClass('active');
				$(this).addClass('active');
			});
		}
	}


	if ($("#anime").length) {
		new Vivus('anime', {
		    type: 'delayed',
		    duration: 100,
		    file: file,
		    animTimingFunction: Vivus.EASE
		});
	}

});