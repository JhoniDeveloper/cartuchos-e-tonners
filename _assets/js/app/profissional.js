jQuery(document).ready(function($) {

	$('.open-profissionais').click(function() {
		if (getCookie('profissional')) {
			window.location.href = vuelo.url+"/profissional/";
		} else {
			$("#modal-profissionais").modal("show");
			return false;
		}
	});

	$("#profissional_form").validate({
		submitHandler: function(form) {
			var button = jQuery(form).find('button[type=submit]').html();
			jQuery(form).find('button[type=submit]').html('<i class="fa fa-circle-o-notch fa-spin"></i>'+aguarde).attr({
				disabled: 'disabled'
			});			
			jQuery.ajax({
				url : vuelo.template + "/inc/form-profissionais.php",
				type : 'post',
				dataType: 'json',
				data : jQuery(form).serialize(),
				success : function( response ) {
					if (response.status == "success") {
						window.location.href = vuelo.url+"/profissional/";
						document.cookie="profissional=Cadastrado";
					} else {
						alert("Estamos em manutenção, tente novamente mais tarde.");
						jQuery(form).find('button[type=submit]').html(button).removeAttr('disabled');
					}
				}
			});	
			return false;
		}
	});


});



function getCookie(key) {
	var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	return keyValue ? keyValue[2] : null;
}

function setCookie(name, value, duration) {
        var cookie = name + "=" + escape(value) +
        ((duration) ? "; duration=" + duration.toGMTString() : "");
        document.cookie = cookie;
}