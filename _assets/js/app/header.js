jQuery(document).ready(function($) 
{

	$('#menu-item-37').append('<span></span>');

	$.fn.extend({
	    animateCss: function (animationName) {
	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        this.addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);
	        });
	    }
	});

	jQuery(".button-responsive").click(function( event ){		
		$(this).toggleClass("active");
		$(".relative .custom-menu .row").toggleClass("active");
		$(".button").toggleClass("back-white");
	});

	jQuery(".whats-action").click(function( event ){
		$(this).toggleClass("active");
	});	

	$(".scrolTop").click(function( event ){

		$('body,html').animate({scrollTop: 0}, 2000);	

	});

	if(jQuery('.identify-home-js').text() === "homejs"){
		$(window).scroll(function() {
			if (!$("body").hasClass('single-product')) {
				if ($(this).scrollTop() > 0){
					$('#produtos-header').slideUp();
					$('#menu-item-37 span').hide();
				}  
				if ($(this).scrollTop() > 280){  
					$('#menu_superior').slideUp();
					$("body").addClass('menu-fix');
					$(".display_social").addClass("fadeIn animated");
					$(".heder-menu").addClass("fadeIn animated");
				}
				else {
					$('#menu_superior').slideDown();
					$('div#menu_inferior').slideDown();
				  	$("body").removeClass('menu-fix');
				  	$(".display_social").removeClass("fadeIn animated");
				}
				$('.menu_normal li.produtos-header').removeClass('active');	
			} else {
				if ($(this).scrollTop() < 200){
					$('nav.nav-highlight').removeClass('scroll_move');	
				}  
				if ($(this).scrollTop() > 200){  
					$("nav.nav-highlight").addClass('scroll_move');
				}	
			}			
	  	});
	}else
	{
		$(window).scroll(function() {
			if (!$("body").hasClass('single-product')) {
				if ($(this).scrollTop() > 0){
					$('#produtos-header').slideUp();
					$('#menu-item-37 span').hide();
				}  
				if ($(this).scrollTop() > 30){  
					$('#menu_superior').slideUp();
					$("body").addClass('menu-fix');
				}
				else {
					$('#menu_superior').slideDown();
					$('div#menu_inferior').slideDown();
				  	$("body").removeClass('menu-fix');
				}
				$('.menu_normal li.produtos-header').removeClass('active');	
			} else {
				if ($(this).scrollTop() < 200){
					$('nav.nav-highlight').removeClass('scroll_move');	
				}  
				if ($(this).scrollTop() > 200){  
					$("nav.nav-highlight").addClass('scroll_move');
				}	
			}
			
	  	});		
	}
  	$('.menu_normal li.produtos-header a').click(function() {
  		if ($('#menu_superior').is(":hidden")) {
  			$('#menu-item-37 span').css('top', '-32px');
  		} else {
  			$('#menu-item-37 span').css('top', '-62px');
  		}
  		$('#menu-item-37 span').toggle();
  		$('#produtos-header').slideToggle().css('display', 'flex');
  		$('.menu_normal li.produtos-header').toggleClass('active');
  		return false;
  	});

  	$('#produtos-header .content img').hover(function() {
  		$('#produtos-header .content img').css('opacity', '0.7');
  		$(this).css('opacity', '1');
  		$(this).animateCss('pulse');
  	}, function() {
  		$('#produtos-header .content img').css('opacity', '1');
  	});

  	/*-------- HAMBURGUER ----------*/
	$( ".hamburger" ).click(function() {
		$( ".mobile_menu .opcoes_inferior" ).slideToggle( "slow", function() {
			$( ".hamburger" ).hide();
			$( ".cross" ).addClass("show-menu");
		});
	});

	$( ".cross" ).click(function() {
		$( ".mobile_menu .opcoes_inferior" ).slideToggle( "slow", function() {
			$( ".cross" ).removeClass("show-menu");
			$( ".hamburger" ).show();
		});
	});

	jQuery("#close .outer").click(function(event) {
		jQuery("#produtos-header").slideUp();
		$('.menu_normal li.produtos-header').removeClass('active');	
	});
	/*-------- HAMBURGUER ----------*/

	/*-------- 	Sub Menus ----------*/
	var conteudo = jQuery('.mobile_menu li.produtos-header').html();
	jQuery(".mobile_menu .produtos-header").html(
		'<div class="align_icons_menuchildren">'+
			'<i class="fa fa-angle-right" aria-hidden="true"></i>'+
		'</div>' + conteudo);

	jQuery('.mobile_menu .produtos-header .sub-menu').slideUp();

	jQuery(".mobile_menu li.produtos-header").click(function(){

		jQuery(".align_icons_menuchildren i").toggleClass('fa-angle-right fa-angle-down');

		jQuery('.produtos-header .sub-menu').slideToggle();	
	});
	/*-------- 	Sub Menus ----------*/

	jQuery(".modal-cart-open, .modal-cart-close").click(function(event) {
		jQuery(".modal-cart").slideToggle();
		return false;
	});
});