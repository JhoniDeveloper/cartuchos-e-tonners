<?php 

#
# Distribuidores - estados
#
add_action('wp_ajax_nopriv_estado_ajax_search','estado_ajax_search');
add_action('wp_ajax_estado_ajax_search','estado_ajax_search');

function estado_ajax_search() {
	$query = $_POST['term'];
	$args = array(
		'post_type' => 'distribuidores',
		'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'estado_distribuidor',
				'value' => $query,
				'compare' => '=='
				)
			),
		);

	$query = new WP_Query( $args );

	  // display results
	if($query->have_posts()){

		while ($query->have_posts()) {
			$query->the_post();
			?>
			<div class="distribuidor col-sm-6 col-md-4">
				<h2><?php the_title(); ?></h2>
				<?php echo get_field("cidade_distribuidor", get_the_ID()); ?>
				<?php the_content(); ?>
			</div>
			<?php
		}
	}else{
		?>
		<div class="distribuidor">
			<p>
				<?php _e("Nenhum resultado encontrado. Você pode encontrar os produtos na nossa", "vuelo"); ?>
				<a href="http://loja.membracel.com.br" target="_blank"><?php _e("loja online.", "vuelo"); ?></a>
			</p>
		</div>
		<?php
	}
	wp_reset_query();

	die();
}

#
# Distribuidores - cidades
#
add_action('wp_ajax_nopriv_cidade_ajax_search','cidade_ajax_search');
add_action('wp_ajax_cidade_ajax_search','cidade_ajax_search');

function cidade_ajax_search() {
	$query = $_POST['term'];
	$args = array(
		'post_type' => 'distribuidores',
		'orderby' => 'title',
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key' => 'cidade_distribuidor',
				'value' => $query,
				'compare' => '=='
				)
			),
		);

	$query = new WP_Query( $args );

	  // display results
	if($query->have_posts()){

		while ($query->have_posts()) {
			$query->the_post();
			?>
			<div class="distribuidor col-md-4">
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</div>
			<?php
		}
	}else{
		?>
		<div class="distribuidor">
			<p>
				<?php _e("Nenhum resultado encontrado. Você pode encontrar os produtos na nossa", "vuelo"); ?>
				<a href="http://loja.membracel.com.br" target="_blank"><?php _e("loja online.", "vuelo"); ?></a>
			</p>
		</div>
		<?php
	}
	wp_reset_query();

	die();
}

	//Add função ajax - LoadMore
add_action('wp_ajax_nopriv_loadmore_ajax','loadmore_ajax');
add_action('wp_ajax_loadmore_ajax','loadmore_ajax');

function loadmore_ajax() {

	global $post;

	$query = array(
		// 'post_type' 		=> $_POST["type"],
		'posts_per_page' 	=> '5',
		'paged' 			=> $_POST["paged"]
	);

	if ($_POST["type"] == "post") {
		$query["post_type"] = "post";
		$sticky = get_option( 'sticky_posts' );
		$query["post__not_in"] = $sticky;
		$query["ignore_sticky_posts"] = true;
	}

	if ($_POST["type"] == "category") {
		$query["post_type"]		= "post";
		$query["category__in"] 	= $_POST["term"];
	}

	if ($_POST["type"] == "search") {
		$query["s"] 	= $_POST["term"];
		if ($_POST["filter"]) {
			$query["post_type"]	= $_POST["filter"];
		}
	}

	query_posts($query);

	if (have_posts()) {
		while (have_posts()) {
			the_post();
			if ($post->post_type == "post") {
				get_template_part("inc/loop", "post");
			} else {
				get_template_part("inc/loop", "search");
			}
		}
	}

	wp_reset_query();
	die();
}


	/* Distribuidores Internacionais */
	add_action('wp_ajax_nopriv_internacionais_ajax_search','internacionais_ajax_search');
	add_action('wp_ajax_internacionais_ajax_search','internacionais_ajax_search');

	function internacionais_ajax_search() {
		$term = $_POST['term'];
		$args = array(
            'post_type' => 'distribuidores_inter',
            'posts_per_page' => -1,
            'orderby' => 'title',
			'order' => 'ASC',
            'tax_query' => array(
				array(
					'taxonomy' => 'taxonomy_dist_inter',
					'terms'    => $term,
					'field'    => 'slug'
				)
			)
        );

		$intquery = new WP_Query( $args );

		  // display results
		if($intquery->have_posts()){

			while ($intquery->have_posts()) {
				$intquery->the_post();
				?>
				<div class="distribuidor distribuidor-int col-sm-6 col-md-4">
					<h2><?php echo get_field('cidade_inter'); ?></h2>
					<p><?php the_title(); ?></p>
					<?php the_content(); ?>
				</div>
				<?php
			}
		}else{
			?>
			<div class="distribuidor distribuidor-int">
				<p>
					<?php _e("Nenhum resultado encontrado. Você pode encontrar os produtos na nossa", "vuelo"); ?>
					<a href="http://loja.membracel.com.br" target="_blank"><?php _e("loja online.", "vuelo"); ?></a>
				</p>
			</div>
			<?php
		}
		wp_reset_query();

		die();
	}

?>