<?php
	if (empty($_POST["url"])) {
		get_header();
	}
	the_post();
?>

<article class="singlepost">
	<div class="nav-bar">
		<div class="row">
			<div class="col-md-3 col-sm-3">
				
				<button class="single-close <?php echo (empty($_POST["url"])) ? "with-single": ""; ?>"> 
					<div class="effect">
					  	<div class="inner">
					    	<label><?php _e("Fechar", "vuelo"); ?></label>
					  	</div>
					</div>
				</button>

				<h4 class="single-date">
					<i class="fa fa-calendar"></i><?php the_time("j/m/Y"); ?>
				</h4>
			</div>
			<div id="title" class="col-md-6 col-sm-9">
				<div class="single-title">
					<h5><?php the_category(", "); ?></h5>
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="col-md-3 single-social-collunm">
				<nav class="single-social">
					<span><?php _e("COMPARTILHE", "vuelo"); ?>: </span>
					<?php if (wp_is_mobile()): ?>
						<a href="whatsapp://send?text=<?php echo urlencode(get_permalink($post->ID)); ?>" target="_blank" class="fa fa-whatsapp"></a>
					<?php endif ?>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>" target="blank" class="fa fa-facebook"></a>
					<a href="mailto:?&body=<?php echo $post->post_title." - ".get_permalink($post->ID); ?>" target="blank" class="fa fa-envelope"></a>
				</nav>
			</div>
		</div>

	</div>

	<div class="single-content">
		
		<div class="loader">
			<div class="stick1"></div>
			<div class="stick2"></div>
			<div class="stick3"></div>
			<div class="stick4"></div>
		</div>

		<div class="row">
			
			<div class="col-md-6 col-md-offset-3">

				<?php if (has_post_thumbnail()): ?>
					<div class="single-post-image-hightlight">
						<?php the_post_thumbnail("full"); ?>
					</div>
				<?php endif ?>

				<div class="single-post-content">
					<?php the_content(); ?>
				</div>
				
				<?php if (get_the_tags($post->ID)): ?>
				<div class="single-tags">
					<i class="fa fa-tags"></i>
					<?php the_tags("", "" ); ?>
				</div>
				<?php endif ?>

				<h2 class="title-comments"><?php _e("COMENTE ESTA PUBLICAÇÃO", "vuelo"); ?></h2>
				<div class="fb-comments" data-href="<?php the_permalink() ?>" data-numposts="5" width="100%"></div>

			</div>
		</div>
	</div>
</article>

<?php
	if (empty($_POST["url"])) {
		get_footer();
	}
?>
